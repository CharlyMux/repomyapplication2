package com.bbva.myapplication2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val button1=findViewById<Button>(R.id.btnOtraAct)
        val button2=findViewById<Button>(R.id.btnMensaje)

        button1.setOnClickListener{
            val intent= Intent(this, SecondActivity::class.java)
            startActivity(intent)
        }
        button2.setOnClickListener{
            Toast.makeText(this, "Hola dio Clck al boton", Toast.LENGTH_SHORT).show()
        }

    }
}